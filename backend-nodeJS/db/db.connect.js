const mysql = require("mysql");

const connection = mysql.createConnection({
  host: "localhost",
  port: "3500",
  database: "resume",
  user: "mysql",
  password: "mysql",
  multipleStatements: true
});

connection.queryAsync = (...args) => {
  const promise = new Promise((resolve, reject) => {
    const cb = (error, data) => {
      if (error) {
        reject(error);
      } else {
        resolve(data);
      }
    };
    connection.query.call(connection, ...args, cb);
  });
  return promise;
};

connection.connect(function (err) {
  if (err) {
    return console.error("Ошибка: " + err.message);
  } else {
    console.log("Подключение к серверу MySQL успешно установлено");
  }
});

module.exports = connection;
