const express = require('express');
const router = express.Router();
const userControllers = require('../controllers/users.controllers');


router.post('/', userControllers.createUser);

router.get('/', userControllers.getAllUsers);

router.get('/:id', userControllers.findUserById);

router.put('/:id', userControllers.updateUser);

router.delete('/:id', userControllers.deleteUser);

module.exports = router;