class User {
    constructor({ fullName, image, position, phone, email, sex, geoPoint, age, sections = [] }) {
        
        this.fullName = fullName;
        this.image = image;
        this.position = position;
        this.phone = phone;
        this.email = email;
        this.sex = sex;
        this.geoPoint = geoPoint;
        this.age = age;
        this.sections = sections.map(section => new Section(section));
    }
}

class Section {
    __type = 'section';
    constructor({ title, id, itemsSection = [] }) {
        this.id = id;
        this.title = title;
        this.itemsSection = itemsSection.map(sectionItem => new SectionItem(sectionItem));
    }
}

class SectionItem {
    __type = 'sectionItem';
    constructor({ id,title, subTitle, from, to, stringArray = [] }) {
        this.id = id;
        this.title = title;
        this.subTitle = subTitle;
        this.from = from;
        this.to = to;
        this.stringArray = stringArray.map(string => string);
    }
}


module.exports = User;