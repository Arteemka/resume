const express = require('express');
const routerUsers = require('./routes/users.routes');
//require('./storages/readDataFromFile');
//require('./db/db.connect');
const PORT = process.env.PORT || 3333;
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');

app.use(cors());

app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ extended: true,limit: '10mb' }));
app.use('/api/users', routerUsers);


const server = app.listen(PORT, function() {
    console.log('Сервер пашет на порту: ' + server.address().port);
})