const usersStorage = require('../storages/usersStorage.storages');
const User = require('../models/user.models');

exports.createUser = async(req, res) => {
    const userData = await usersStorage.addItem(new User(req.body));

    res.send(userData);
    res.sendStatus(200);
}

exports.getAllUsers = async(req, res) => {
    const getAllUsers = await usersStorage.getItems();
   
    res.send(getAllUsers);
}

exports.updateUser = async(req, res) => {
    
    const id = req.params.id;
    const updatedValueUser = await usersStorage.updateItem(id,new User(req.body));

    res.send(updatedValueUser);
}

exports.deleteUser = async(req, res) => {
    await usersStorage.deleteItem(req.params.id);

    res.status(200);
}

exports.findUserById = async(req, res) => {
    const id = req.params.id;
    const getUser = await usersStorage.getItem(id);

    res.send(getUser);
}