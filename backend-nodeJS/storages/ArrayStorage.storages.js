const uuidv4 = require('uuid');
// let uniqueItemId = 0;

// function uniqueId() {
//     return uniqueItemId++;
// }

class ArrayStorage {
    _items = [];

    async getItems() {
        return this._items.sort((a, b) => a.id - b.id);;
    }

   async addItem(item) {
        //item.id = uniqueId();
        item.id = uuidv4.v4();
        this._items.push(item);
        return item;
    }

    async hasItemWithId(id) {
        return this._items.some(item => item.id === id)
    }

    async deleteItem(id) {
        if (!this.hasItemWithId(id)) return false;

        this._items = this._items.filter(item => item.id !== id);

        return true;
    }

  async  getItem(id) {
        return this._items.find(item => item.id === id);
    }

   async updateItem(id, newObject) {
        const item = await this.getItem(id);
       
        Object.keys(newObject || {}).forEach(key => {
            if (item.hasOwnProperty(key)) item[key] = newObject[key];
        });


        return item;
    }
}

module.exports = ArrayStorage;