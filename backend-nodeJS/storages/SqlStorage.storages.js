let connection = require(".././db/db.connect");

class SqlStorage {
  async getItems() {
    const sql = `
    SELECT Users.id,Users.fullName, CONCAT('[',group_concat(sect.section),']') as sections
      from Users
        left join(
          select user_id, CONCAT ( CONCAT( '{','"id"',':',sections_id,', ' '"title"',':',sectionItems.title ),
          ', ',IFNULL(CONCAT( 'SectionsItems:',sectionsitems),'') ,'}') as section
              from (
                  SELECT Sections.user_id as user_id, Sections.id as sections_id, Sections.title as title, 
                  CONCAT( '[', group_concat( CONCAT( '{"id":"', SectionItems.id,'"', ',"subTitle":"', 
                  SectionItems.subTitle, '"', ',"section_id":"', SectionItems.section_id,'"}' ) ), ']') as sectionsitems
                      FROM Sections left JOIN SectionItems ON Sections.id = SectionItems.section_id GROUP by Sections.id
    ) as sectionItems) as sect on Users.id = sect.user_id  where id = ${id} group by id
  `;
    const items = await connection.queryAsync(sql);

    // const sql = `SELECT * from Users;`;
    // let items = await connection.queryAsync(sql);
    // const sections = await connection.queryAsync("SELECT * from Sections");
    // let sectionItems = await connection.queryAsync(
    //   `select * from SectionItems`
    // );

    // const outputItems = items.map((item) => {
    //   return {
    //     id: item.id,
    //     fullName: item.fullName,
    //     position: item.position,
    //     email: item.email,
    //     sections: [],
    //   };
    // });

    // outputItems.forEach((element) => {
    //   sections.forEach((item) => {
    //     if (element.id == item.user_id) {
    //       element.sections.push({
    //         id: item.id,
    //         title: item.title,
    //         user_id: item.user_id,
    //         SectionItems: [],
    //       });
    //     }
    //   });
    // });

    // outputItems.forEach((item) =>
    //   item.sections.forEach((item1) => {
    //     sectionItems.forEach((item2) => {
    //       if (item1.id === item2.section_id) {
    //         item1.SectionItems.push(item2);
    //       }
    //     });
    //   })
    // );

    // return outputItems;
    return items;
  }

  async addItem(item) {
    connection.beginTransaction(async (err) => {
      if (err) {
        throw err;
      }

      const userValue = [item.fullName, item.position, item.email];
      try {
        const resultUser = await connection.queryAsync(
          "INSERT INTO Users(fullName,position, email) VALUES (?)",
          [userValue]
        );
        let user_id = resultUser.insertId;
        let sections = item.sections.map(function (item) {
          return [item.title, user_id];
        });

        if (sections.length === 0) return false;

        const resultSection = await connection.queryAsync(
          "INSERT INTO Sections (title,user_id) VALUES ? ",
          [sections]
        );

        let section_id = resultSection.insertId;
        let values = item.sections
          .map((items, index) => {
            return items.itemsSection.map((item) => {
              return [
                item.title,
                item.subTitle,
                item.from,
                item.to,
                section_id + index,
              ];
            });
          })
          .flat();

        if (values.length === 0) return false;

        await connection.queryAsync(
          "INSERT INTO SectionItems (title,subTitle,`from`,`to`,section_id) VALUES ? ",
          [values]
        );
      } catch (error) {
        await connection.rollback(function () {
          throw error;
        });
      }

      await connection.commit(function (err) {
        if (err) {
          connection.rollback(function () {
            throw err;
          });
        }

        console.log("Transaction Complete.");
        return true;
      });
    });
  }

  async deleteItem(id) {
    let sql = `DELETE FROM Users WHERE id = ?`;

    return connection.queryAsync(sql, id);
  }

  async getItem(id) {
    const sql = `
      SELECT Users.id,Users.fullName, CONCAT('[',group_concat(sect.section),']') as sections
        from Users
          left join(
            select user_id, CONCAT ( CONCAT( '{','"id"',':',sections_id,', ' '"title"',':',sectionItems.title ),
            ', ',IFNULL(CONCAT( 'SectionsItems:',sectionsitems),'') ,'}') as section
                from (
                    SELECT Sections.user_id as user_id, Sections.id as sections_id, Sections.title as title, 
                    CONCAT( '[', group_concat( CONCAT( '{"id":"', SectionItems.id,'"', ',"subTitle":"', 
                    SectionItems.subTitle, '"', ',"section_id":"', SectionItems.section_id,'"}' ) ), ']') as sectionsitems
                        FROM Sections left JOIN SectionItems ON Sections.id = SectionItems.section_id GROUP by Sections.id
      ) as sectionItems) as sect on Users.id = sect.user_id  where id = ${id} group by id
    `;

    const item = await connection.queryAsync(sql);

    return item;
  }

  async updateItem(id, newObject) {
    connection.beginTransaction(async (err) => {
      if (err) {
        throw err;
      }
      const sql = `UPDATE Users SET fullName =? ,position=?, email=? WHERE id = ${id};`;
      let values = [newObject.fullName, newObject.position, newObject.email];

      try {
        await connection.queryAsync(sql, values);

        let queriesSections = "";

        newObject.sections.forEach(function (item) {
          if (item.id === undefined) {
            queriesSections += connection.format(
              "INSERT INTO Sections (title,user_id) VALUES (?,?); ",
              [item.title, id]
            );
          }

          queriesSections += connection.format(
            `UPDATE Sections SET title = ? WHERE id = ?; `,
            [item.title, item.id]
          );
        });
        await connection.queryAsync(queriesSections);

        let queriesItemsSection = "";

        newObject.sections.forEach((section) => {
          section.itemsSection.forEach((sectionItem) => {
            if (sectionItem.id === undefined) {
              queriesItemsSection += connection.format(
                "INSERT INTO SectionItems (title,subTitle,`from`,`to`,section_id) VALUES (?,?,?,?); ",
                [sectionItem.subTitle, section.id]
              );
            }

            queriesItemsSection += connection.format(
              "UPDATE SectionItems SET title = ?, subTitle = ?, `from` = ?, `to` = ? WHERE id = ?; ",
              [
                sectionItem.title,
                sectionItem.subTitle,
                sectionItem.from,
                sectionItem.to,
                sectionItem.id,
              ]
            );
          });
        });

        await connection.query(queriesItemsSection);
      } catch (err) {
        await connection.rollback(function () {
          throw error;
        });
      }

      await connection.commit(function (err) {
        if (err) {
          connection.rollback(function () {
            throw err;
          });
        }

        console.log("Transaction Complete.");
        return true;
      });
    });
  }
}

module.exports = SqlStorage;
