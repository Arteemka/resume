const usersStorage = require('./usersStorage.storages');
const User = require('../models/user.models');
const fs = require('fs');

fs.readFile('./db/database.db.json', (err, data) => {
    if (err) throw err;
    const usersFromDB = JSON.parse(data);
    usersFromDB.forEach(user => {
        usersStorage.addItem(new User(user));
    });
});

module.exports = fs;