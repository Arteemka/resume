const ArrayStorage = require('./ArrayStorage.storages.js');
const SqlStorage = require('./SqlStorage.storages');

//const usersStorage = new ArrayStorage;
const usersStorage = new SqlStorage;

module.exports = usersStorage;