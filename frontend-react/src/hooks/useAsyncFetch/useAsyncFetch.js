import { useState, useEffect } from "react";

export default function useAsyncFetch(promisedFn, settings={fetchImmediatly:true}) {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const fetchData = async () => {
    try {
      setIsLoading(true);
      setError(undefined);
      const result = await promisedFn();
      setData(result);
      setIsLoading(true);
    } catch (e) {
      setError(e);
      console.error(e);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    settings.fetchImmediatly && fetchData();
  }, [promisedFn, settings]);

  return {
    data,
    isLoading,
    error,
    hasError: Boolean(error),
    fetch: fetchData,
  };
}
