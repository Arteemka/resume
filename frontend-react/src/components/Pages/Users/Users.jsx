import { Link } from "react-router-dom";

import iconUser from "../../../image/user.png";
import iconJob from "../../../image/job.png";
import iconEmail from "../../../image/mail.png";
import iconEdit from "../../../image/edit.png";
import iconDelete from "../../../image/deleteUser.png";

import "./Users.css";

const Users = ({ data, deleteItem }) => {
  
  return (
    data &&
    data.map((user) => (
      <Link to={"/" + user.id} key={user.id} style={{ textDecoration: "none" }}>
        <div className="block_user" >
          {user.image ? (
            <img className="uploaded_photo" src={user.image} />
          ) : (
            <div className="block_user__image">
              <span className="block_user__text">
                {user.fullName
                  .split(" ")
                  .map((w) => w[0])
                  .join("")}
              </span>
            </div>
          )}
          <div className="block_user_info">
            <div>
              <img src={iconUser} />
              {user.fullName}
            </div>
            <div>
              <img src={iconJob} />
              {user.position}
            </div>
            <div>
              <img src={iconEmail} />
              {user.email}
            </div>
          </div>
          <div className="block_user__modifications">
            <Link to={"/" + user.id + "/edit"}>
              <div className="block_modication__edit" editItem={"return false"}>
                <img src={iconEdit} />
              </div>
            </Link>
            <div
              className="block_modication__delete"
              onClick={(event) => {
                event.preventDefault();
                deleteItem( user.id)}}
            >
              <img src={iconDelete} />
            </div>
          </div>
        </div>
      </Link>
    ))
  );
};

export default Users;
