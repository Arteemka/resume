import "./SectionsItems.css";

const SectionsItems = ({ title, subTitle, from, to, stringArray }) => {
  return (
    <div className="item_section__body">
      {title && <div className="title">{title}</div>}
      {subTitle && <div className="SubTitle">{subTitle}</div>}
      {(from || to) && <div className="timeWork">{from} - {to}</div>}
      <ul className="listItems_section">
        {stringArray &&
          stringArray.map((item, index) => {
            return (
              <li key={index} className="item_section">
                <span>{item}</span>
              </li>
            );
          })}
      </ul>
    </div>
  );
};

export default SectionsItems;
