import SectionsItems from "./SectionsItems";
import "./Section.css";

const Section = ({ title, isOpen, id, sectionItems, toggleClass }) => {
  return (
    <div className="section_group">
      <div className={isOpen} onClick={() => toggleClass(id)}>
        <button id={id} className="button_title">
          {title}
        </button>
      </div>
      <div className="section_body">
        {sectionItems &&
          sectionItems.map((item,index) => {
            return (
              <SectionsItems
                key={index}
                title={item.title}
                subTitle={item.subTitle}
                from={item.from}
                to={item.to}
                stringArray={item.stringArray}
              />
            );
          })}
      </div>
    </div>
  );
};

export default Section;
