import { React, useState, useEffect } from "react";

import useAsyncFetch from "../../../hooks/useAsyncFetch/useAsyncFetch"
import APIService from '../../../services/APIService';
import Section from "./Section.jsx";

import "./User.css";



const User = (props) => {
  const [isOpen, setIsOpen] = useState(null);
  const {data:dataUser,isLoading,error,hasError} = useAsyncFetch(()=>APIService.getResumeUser(props.match.params.id));

  const toggleClass = (index) => {
    setIsOpen(index);
  };

  return (
    <div className="container_user">
      {dataUser && (
        <div className="container_resume">
          <div className="container_info__person">
            <div className="block__image">
              {dataUser.image ? (
                <img className="block_user__photo" src={dataUser.image} />
              ) : (
                  <div className="block_default__image">
                    <span className="block_firstLetters__FIO">
                      {dataUser.fullName
                        .split(" ")
                        .map((w) => w[0])
                        .join("")}
                    </span>
                  </div>
                )}
            </div>
            <div className="info_person">
              <div className="info_person_block">
                <div className="fullName">{dataUser.fullName}</div>
                <div className="position">{dataUser.position}</div>
                <div className="number_phone">
                  <span className="icon-phone"></span>
                  {dataUser.phone}
                </div>
                <div className="email">
                  <span className="icon-mail"></span>
                  {dataUser.email}
                </div>
                <div className="geo_point">
                  <span className="icon-geo-point"></span>
                  {dataUser.geoPoint}
                </div>
              </div>
            </div>
          </div>
          <div className="container_section">
            {dataUser.sections &&
              dataUser.sections.map(function (item, index) {
                return (
                  <Section
                    id={index}
                    key={index}
                    isOpen={
                      isOpen === index
                        ? "section_title icon-arrow-down icon-arrow-up"
                        : "section_title icon-arrow-down"
                    }
                    toggleClass={toggleClass}
                    title={item.title}
                    sectionItems={item.itemsSection}
                  />
                );
              })}
          </div>
        </div>
      )}
    </div>
  );
};

export default User;
