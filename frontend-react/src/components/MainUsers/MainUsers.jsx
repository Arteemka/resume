import { React, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import useAsyncFetch from "../../hooks/useAsyncFetch/useAsyncFetch";
import APIService from "../../services/APIService";
import Users from "../Pages/Users/Users";
import Loading from "../Loading/Loading";

import "./MainForm.css";

const MainUsers = () => {
  const usersAction = useAsyncFetch(() => APIService.getAllResumesUsers());

  const deleteItem = async (id) => {
    try {
      await APIService.deleteResumeUser(id);
      await usersAction.fetch();
      Notification.success("Успех", "Резюме успешно удалено!");
    } catch (error) {
      Notification.success("Ошибка", "Упс, произошла ошибка удаления!");
    }
  };

  return (
    <>
      {/* {!usersAction.isLoading ? ( */}
        <div className="container">
          <div className="container__content">
            <Users data={usersAction.data} deleteItem={deleteItem} />
            <Link to={"/create"}>
              <button className="container__button-add">ДОБАВИТЬ</button>
            </Link>
          </div>
        </div>
      {/* ) : (
        <Loading />
      )} */}
    </>
  );
};

export default MainUsers;
