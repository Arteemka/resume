import "./StringArray.css";

const StringArray = ({  setSkills, addSkills }) => {
  const onChangeHendler = (event) => {
    setSkills(event.target.value);
  };
  return (
    <>
      <div className="sectionItem_edit__stringArray">
        <label htmlFor="stringArray">DESCRIPTION</label>
        <textarea
          className="sectionItem_edit_textarea__StringArray"
          name="stringArray"
          id="stringArray"
          type="text"
          onChange={onChangeHendler}
        />
      </div>

      <div>
        <button
          className="sectionItem__addSkills"
          type="button"
          onClick={addSkills}
        >
          ADD DESCRIPTION{" "}
        </button>
      </div>
    </>
  );
};

export default StringArray;
