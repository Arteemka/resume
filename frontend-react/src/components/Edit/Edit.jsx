import React, { useState, useEffect, useRef } from "react";
import { Redirect } from "react-router";
import useAsyncFetch from "../../hooks/useAsyncFetch/useAsyncFetch";
import APIService from "../../services/APIService";

import "../../lib/Notification";
import { v4 as uuidv4 } from "uuid";

import Section from "./Sections";
import Loading from "../Loading/Loading";

import plus from "../../image/plus.png";
import deleteButton from "../../image/deleteImage.png";

import "./Edit.css";

const sleep = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

const getResume = async (data) => {
  await sleep(2000);
  return !data
    ? {
        sections: [
          {
            id: 0,
            title: "WORK",
            itemsSection: [{ id: 0 }],
          },
          {
            id: 1,
            title: "Education",
            itemsSection: [{ id: 1 }],
          },
          {
            id: 2,
            title: "Skills",
            itemsSection: [{ id: 2 }],
          },
        ],
      }
    : data;
};

const Edit = (props) => {
  const [resume, setResume] = useState(null);
  const [redirect, setRedirect] = useState(false);
  const fileField = useRef(null);
  const usersAction = useAsyncFetch(() =>
    APIService.getAllResumesUsers(props.match.params.id)
  );

  useEffect(() => {
    getResume(usersAction.data)
      .then((resume) => {
        setResume(resume);
      })
      .catch((err) => console.log(err));
  }, []);

  const onChangeName = ({ target }) => {
    setResume({
      ...resume,
      [target.name]: target.value,
    });
  };

  const addSection = (event) => {
    event.preventDefault();

    setResume({
      ...resume,
      sections: [...(resume.sections || []), { id: uuidv4() }],
    });
  };

  const onChangeSection = (section) => {
    const sectionIndex = resume.sections.findIndex((x) => x.id === section.id);

    setResume({
      ...resume,
      sections: [
        ...resume.sections.slice(0, sectionIndex),
        section,
        ...resume.sections.slice(sectionIndex + 1, resume.sections.length),
      ],
    });
  };

  const addItem = async (event) => {
    event.preventDefault();

    if (resume.id === undefined) {
      try {
        await APIService.createResumeUser(JSON.stringify(resume));
        setRedirect(true);
        Notification.success("Успех", "Данные успешно добавлены!");
      } catch (e) {
        Notification.error("Ошибка", "Ошибка добавления данных!");
      }
    } else {
      try {
        await APIService.updateResumeUser(
          props.match.params.id,
          JSON.stringify(resume)
        );

        setRedirect(true);
        Notification.success("Успех", "Данные успешно обнавлены!");
      } catch (e) {
        Notification.error("Ошибка", "Ошибка обновления данных!");
      }
    }
  };

  const deleteItemFromSections = (event) => {
    setResume({
      ...resume,
      sections: resume.sections.filter(
        (item) => item.id !== parseInt(event.target.id)
      ),
    });
  };

  const addImage = ({ target }) => {
    const files = Array.from(target.files);
    const reader = new FileReader();

    reader.onloadend = function () {
      setResume({
        ...resume,
        [target.name]: reader.result,
      });
    };
    reader.readAsDataURL(files[0]);
  };

  const deleteImage = () => {
    setResume({
      ...resume,
      image: null,
    });
  };

  if (redirect) {
    return <Redirect to={"/"} />;
  }

  return (
    <>
      {true ? (
        resume && (
          <div className="block_edit">
            <form onSubmit={addItem}>
              <div className="block__modificate_edit">
                {resume.id === undefined ? (
                  <div className="title_modificate">{"Создание"}</div>
                ) : (
                  <div className="title_modificate">{"Редактирование"}</div>
                )}
                <div className="modificate_edit__item">
                  <label htmlFor="fullName">FullName</label>
                  <input
                    name="fullName"
                    id="fullName"
                    className="modificate_edit__fullName"
                    onChange={onChangeName}
                    value={resume.fullName}
                  />
                </div>
                <div className="modificate_edit__item">
                  <label htmlFor="position">Position</label>
                  <input
                    name="position"
                    id="position"
                    className="modificate_edit__position"
                    onChange={onChangeName}
                    value={resume.position}
                  />
                </div>
                <div className="modificate_edit__item">
                  <label htmlFor="phone">Phone</label>
                  <input
                    name="phone"
                    id="phone"
                    className="modificate_edit__phone"
                    onChange={onChangeName}
                    value={resume.phone}
                  />
                </div>
                <div className="modificate_edit__item">
                  <label htmlFor="email">email</label>
                  <input
                    name="email"
                    id="email"
                    className="modificate_edit__email"
                    onChange={onChangeName}
                    value={resume.email}
                  />
                </div>
                <div className="modificate_edit__item">
                  <label htmlFor="sex">Sex</label>
                  <input
                    name="sex"
                    id="sex"
                    className="modificate_edit__sex"
                    onChange={onChangeName}
                    value={resume.sex}
                  />
                </div>
                <div className="modificate_edit__item">
                  <label htmlFor="geoPoint">GeoPoint</label>
                  <input
                    name="geoPoint"
                    id="geoPoint"
                    className="modificate_edit__geoPoint"
                    onChange={onChangeName}
                    value={resume.geoPoint}
                  />
                </div>
                <div className="modificate_edit__item">
                  <label htmlFor="age">Age</label>
                  <input
                    name="age"
                    id="age"
                    className="modificate_edit__age"
                    onChange={onChangeName}
                    value={resume.age}
                  />
                </div>
                <div className="modificate_edit__item">
                  <label htmlFor="image">Изображение</label>

                  <img className="loadedImageUsers" src={resume.image} />
                  <img
                    className="deleteImage"
                    src={deleteButton}
                    onClick={deleteImage}
                  />
                  <input
                    type="file"
                    name="image"
                    id="image"
                    className="chooseFile"
                    ref={fileField}
                    onChange={addImage}
                  />
                </div>
                <div className="sections">
                  {resume.sections &&
                    resume.sections.map((section) => (
                      <Section
                        section={section}
                        key={section.id}
                        onChange={onChangeSection}
                        setResume={setResume}
                        resume={resume}
                        deleteItem={deleteItemFromSections}
                      />
                    ))}

                  <button className="button_addSection" onClick={addSection}>
                    <img src={plus} />
                  </button>
                </div>
              </div>
              <input
                type="submit"
                className="button_modify"
                value={resume.id === undefined ? "Создание" : "Редактирование"}
              />
            </form>
          </div>
        )
      ) : (
        <Loading />
      )}
    </>
  );
};

export default Edit;
