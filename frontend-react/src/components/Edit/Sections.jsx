import React from "react";
import ItemsSections from "./ItemsSection";
import { v4 as uuidv4 } from 'uuid';

import plus from "../../image/plus.png";
import deleteButton from "../../image/deleteImage.png";

import "./Sections.css";



const Section = ({
  
  deleteItem,
  section,
  onChange,
  setResume,
  resume,
}) => {
  const onFieldChange = ({ target }) => {
    onChange({
      ...section,
      [target.name]: target.value,
    });
  };

  const addItemSection = (event) => {
    event.preventDefault();
    const sectionItemIndex = resume.sections.findIndex(
      (x) => x.id === section.id
    );

    setResume({
      ...resume,
      sections: [
        ...resume.sections.slice(0, sectionItemIndex),
        {
          ...section,
          itemsSection: [...(section.itemsSection || []), { id: uuidv4() }],
        },
        ...resume.sections.slice(sectionItemIndex + 1, resume.sections.length),
      ],
    });
  };

  const onChangeItemSection = (sectionItem) => {
    const itemIndex = section.itemsSection.findIndex(
      (x) => x.id === sectionItem.id
    );
    const sectionItemIndex = resume.sections.findIndex(
      (x) => x.id === section.id
    );


    console.log(itemIndex, sectionItemIndex)
    setResume({
      ...resume,
      sections: [
        ...resume.sections.slice(0, sectionItemIndex),
        {
          ...section,
          itemsSection: [
            ...section.itemsSection.slice(0, itemIndex),
            sectionItem,
            ...section.itemsSection.slice(
              itemIndex + 1,
              section.itemsSection.length
            ),
          ],
        },
        ...resume.sections.slice(sectionItemIndex + 1, resume.sections.length),
      ],
    });
  };
  const deleteSectionItem = (event) => {
    const sectionItemIndex = resume.sections.findIndex(
      (x) => x.id === section.id
    );

    setResume({
      ...resume,
      sections: [
        ...resume.sections.slice(0, sectionItemIndex),
        {
          ...section,
          itemsSection: section.itemsSection.filter(
            (item) => item.id !== event.target.id
          ),
        },
        ...resume.sections.slice(sectionItemIndex + 1, resume.sections.length),
      ],
    });
  };

  return (
    <section>
      <div className="headerSection">Секция</div>
      <div className="modificate_edit_section__item">
        <label htmlFor="title">Title</label>
        <input
          name="title"
          id="title"
          className="section_edit__title"
          onChange={onFieldChange}
          value={section.title}
        />
        <span className="button__modificate">
          <button className="button_addItemInSection" onClick={addItemSection}>
            <img src={plus} />
          </button>
          <button
            className="button_deleteItemSection"
            onClick={(event) => deleteItem(event)}
          >
            <img id={section.id} src={deleteButton} />
          </button>
        </span>
        
      </div>

      <div className="ItemsSections">
        {section.itemsSection &&
          section.itemsSection.map((sectionItem) => (
                        
              <ItemsSections
                sectionItem={sectionItem}
                key={sectionItem.id}
                onChange={onChangeItemSection}
                setResume={setResume}
                resume={resume}
                section={section}
                deleteSectionItem={deleteSectionItem}
              />
            
          ))}
      </div>
    </section>
  );
};

export default Section;
