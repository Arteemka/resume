import React, { useState } from "react";
import StringArray from "./StringArray";

import deleteItem from "../../image/deleteImage.png";

import "./ItemsSection.css";

const ItemsSection = ({
  deleteSectionItem,
  section,
  sectionItem,
  onChange,
  setResume,
  resume
}) => {
  const [skill, setSkills] = useState("");
  const onFieldChange = ({ target }) => {
    onChange({
      ...sectionItem,
      [target.name]: target.value,
    });
  };

  const addSkills = () => {
    const findIndexItemSection = resume.sections.findIndex(
      (x) => x.id === section.id
    );
    const findIndexItemsSection = section.itemsSection.findIndex(
      (x) => x.id === sectionItem.id
    );

    setResume({
      ...resume,
      sections: [
        ...resume.sections.slice(0, findIndexItemSection),
        {
          ...section,
          itemsSection: [
            ...section.itemsSection.slice(0, findIndexItemsSection),
            {
              ...sectionItem,
              stringArray: [...(sectionItem.stringArray || []), skill],
            },
            ...section.itemsSection.slice(
              findIndexItemsSection + 1,
              section.itemsSection.length
            ),
          ],
        },
        ...resume.sections.slice(
          findIndexItemSection + 1,
          resume.sections.length
        ),
      ],
    });
  };

  const deleteKeySkill = (event) => {
    const findIndexItemSection = resume.sections.findIndex(
      (x) => x.id === section.id
    );
    const findIndexItemsSection = section.itemsSection.findIndex(
      (x) => x.id === sectionItem.id
    );

    setResume({
      ...resume,
      sections: [
        ...resume.sections.slice(0, findIndexItemSection),
        {
          ...section,
          itemsSection: [
            ...section.itemsSection.slice(0, findIndexItemsSection),
            {
              ...sectionItem,
              stringArray: sectionItem.stringArray.filter(
                (item, index) => index !== event.target.id
              ),
            },
            ...section.itemsSection.slice(
              findIndexItemsSection + 1,
              section.itemsSection.length
            ),
          ],
        },
        ...resume.sections.slice(
          findIndexItemSection + 1,
          resume.sections.length
        ),
      ],
    });
  };

  return (
    <>
      <section>
        <div className="headerSection">Под секция</div>
        <div>
          <label htmlFor="title">Title</label>
          <input
            name="title"
            className="sectionItem_edit__title"
            id="title"
            onChange={onFieldChange}
            value={sectionItem.title }
          />
          <span
            className="deleteItemFromSectionItems"
            onClick={deleteSectionItem}
          >
            <img id={sectionItem.id} src={deleteItem} />
          </span>
        </div>
        <div>
          <label htmlFor="subTitle">SubTitle</label>
          <input
            className="sectionItem_edit__subTitle"
            name="subTitle"
            id="subTitle"
            onChange={onFieldChange}
            value={ sectionItem.subTitle }
          />
        </div>
        <div>
          <label htmlFor="from">from</label>
          <input
            className="sectionItem_edit__from"
            name="from"
            id="from"
            onChange={onFieldChange}
            value={ sectionItem.from }
          />
        </div>
        <div>
          <label htmlFor="to">to</label>
          <input
            className="sectionItem_edit__to"
            name="to"
            id="to"
            onChange={onFieldChange}
            value={sectionItem.to }
          />
        </div>
        <StringArray
          deleteKeySkill={deleteKeySkill}
          sectionItem={sectionItem}
          setSkills={setSkills}
          addSkills={addSkills}
        />
        <div className="outputSkills">
          {sectionItem.stringArray &&
            sectionItem.stringArray.map((item, index) => (
              <div className="otputKeySkill">
                <div className="keySkill" key={index}>
                  {item}
                </div>
                <div
                  onClick={deleteKeySkill}
                  id={index}
                  className="deleteSkill"
                >
                  X
                </div>
              </div>
            ))}
        </div>
      </section>
    </>
  );
};

export default ItemsSection;
