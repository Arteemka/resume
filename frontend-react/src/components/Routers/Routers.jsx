import React from "react";
import { Route } from "react-router";

import MainUsers from "../MainUsers/MainUsers";
import User from "../Pages/User/User";
import Edit from "../Edit/Edit";

const Routers = () => (
  <>
    <Route exact path="/" component={MainUsers} />
    <Route exact={true} path="/:id" component={User} />
    <Route exact={true} path="/:id/edit" component={Edit} />
    <Route exact={true} path="/create" component={Edit} />
  </>
);

export default Routers;
