export default Notification = (function () {
  function showMessage(titleMessage, bodyMessage, background, color) {
    const notification = document.createElement("div");
    const createTitleMessage = document.createElement("div");
    const createBodyMessage = document.createElement("div");


    notification.className = "notification";
    createBodyMessage.className = "body_message";
    createTitleMessage.className = "title_message";

    notification.style.background = background;
    notification.style.color = color;

    createTitleMessage.innerHTML = titleMessage;
    createBodyMessage.innerHTML = bodyMessage;

    document.body.append(notification);
    notification.append(createTitleMessage);
    notification.append(createBodyMessage);


    setTimeout(() => notification.remove(), 5000);
  }

  return {
    success: function (titleMessage, bodyMessage, background = "#affaaf", color = "#228B22") {
      showMessage(titleMessage, bodyMessage, background, color);
    },

    error: function (titleMessage, bodyMessage, background = "#fca69d", color = "#CD5C5C") {
      showMessage(titleMessage, bodyMessage, background, color);
    },
  };
})();

