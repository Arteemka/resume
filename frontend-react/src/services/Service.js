export default class Service {
  constructor(baseUrl) {
    this.baseUrl = baseUrl;
  }

  async getRequsetData({ id, body, method, headers }) {
    
    const url = this.baseUrl + (id === undefined ? "" : id);
    
    try {
      const response = await fetch(url, {
        method: method,
        headers,
        body: body,
      });
      const result = await response.json();
      return result;
    } catch (error) {
      console.log(error);
    }
  }

  async GET(id) {
    return await this.getRequsetData({
      id,
      method: "GET",
    });
  }

  async DELETE(id) {
    return await this.getRequsetData({
      id,
      method: "DELETE",
    });
  }

  async POST(data) {
    return await this.getRequsetData({
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: data,
    });
  }

  async PUT(id, newData) {
    return await this.getRequsetData({
      id,
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: newData,
    });
  }
}
