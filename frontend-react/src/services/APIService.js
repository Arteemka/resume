import Service from "./Service";

class APIService extends Service {
  constructor() {
    super("http://localhost:3333/api/users/");
  }

  async getAllResumesUsers() {
    return await this.GET();
  }

  async getResumeUser(id) {
    return await this.GET(id);
  }
  async deleteResumeUser(id) {
   return await this.DELETE(id);
  }

  async createResumeUser(data) {
    return await this.POST(data);
  }

  async updateResumeUser(id,newData) {
    
    return await this.PUT(id, newData);
  }
}

export default new APIService();
